package utils

import (
	"crypto/sha1"
	"encoding/hex"
)

// Sha1加密
func Sha1s(b []byte) string {
	r := sha1.Sum(b)
	return hex.EncodeToString(r[:])
}
