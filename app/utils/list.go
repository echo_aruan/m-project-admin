package utils

import (
	"reflect"
)

// format list
func FormatList(data interface{}, formatFunc func(map[string]interface{}, interface{})) []map[string]interface{} {
	list := InterfaceToSlice(data)
	result := make([]map[string]interface{}, 0)
	for _, e := range list {
		line := ObjToMap(e)
		formatFunc(line, e)
		result = append(result, line)
	}
	return result
}

// interface to slice
func InterfaceToSlice(i interface{}) []interface{} {
	v := reflect.ValueOf(i)
	if v.Kind() != reflect.Slice {
		return []interface{}{}
	}

	l := v.Len()
	result := make([]interface{}, l)
	for i := 0; i < l; i++ {
		result[i] = v.Index(i).Interface()
	}
	return result
}
