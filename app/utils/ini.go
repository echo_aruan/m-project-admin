package utils

import (
	"github.com/go-ini/ini"
)

const CONFFILE = "./config/conf.ini"

var IniHandler *ini.File

func GetSectionValue(sectionKey string, valueKey string, defaultValue string) string {
	if IniHandler == nil {
		var err error
		IniHandler, err = ini.Load(CONFFILE)
		if err != nil {
			return defaultValue
		}
		IniHandler.BlockMode = false
	}
	value := IniHandler.Section(sectionKey).Key(valueKey).Value()
	if value == "" {
		return defaultValue
	}
	return value
}

/*
example:
	GetSectionValue("", "port", "")  	  // 获取一级kv
	GetSectionValue("server", "port", "") // 获取二级kv
*/
