package utils

import (
	"github.com/labstack/echo"
	"strconv"
)

func GetSkipPage(PageCount int, c echo.Context) (int, int) {
	p := c.QueryParam("p")
	page, err := strconv.Atoi(p)
	if err != nil {
		page = 1
	}

	if page == 0 {
		page = 1
	}

	skip := (page - 1) * PageCount
	return skip, page
}

// 通过两重循环过滤重复元素
func RemoveRepByLoop(slc []string) []string {
	result := []string{} // 存放结果
	for i := range slc {
		flag := true
		for j := range result {
			if slc[i] == result[j] {
				flag = false // 存在重复元素，标识为false
				break
			}
		}
		if flag { // 标识为false，不添加进结果
			result = append(result, slc[i])
		}
	}
	return result
}

func GetEnvDesc() string {
	envDesc := GetSectionValue("env", "desc", "")
	if envDesc == "" {
		envDesc = "未知"
	}
	return envDesc
}
