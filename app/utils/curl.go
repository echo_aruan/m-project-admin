package utils

import (
	"errors"
	"github.com/mikemintang/go-curl"
	"strings"
)

func Post(url string, data map[string]interface{}, headers map[string]string, cookies map[string]string, queries map[string]string) (string, error) {
	req := curl.NewRequest()
	req.SetUrl(strings.TrimSpace(url))
	if nil != headers {
		req.SetHeaders(headers)
	}
	if nil != cookies {
		req.SetCookies(cookies)
	}
	if nil != queries {
		req.SetQueries(queries)
	}
	if nil != data {
		req.SetPostData(data)
	}

	resp, err := req.Post()
	if nil != err {
		return "", err
	}
	if resp.IsOk() {
		return resp.Body, nil
	}
	return "", errors.New("http status not ok")
}
