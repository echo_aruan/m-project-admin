package utils

// 发送文本格式消息
func SendText(content string, phone []string, isAtAll bool) error {
	envName := GetSectionValue("env", "name", "")
	if envName == "" {
		envName = "未知"
	}

	// 本地环境不给发消息
	if envName == "local" {
		return nil
	}

	params := make(map[string]interface{})
	params["msgtype"] = "text"
	conPrefix := "[push_admin]\n环境：" + envName + "\n"
	text := map[string]string{"content": conPrefix + content}
	params["text"] = text
	at := make(map[string]interface{})
	at["atMobiles"] = phone
	at["isAtAll"] = isAtAll
	params["at"] = at
	url := "https://oapi.dingtalk.com/robot/send?access_token=e1a80e0f6b2b5263fe6d9f0a33bcf8fbe372df81fba3f8ba2673489ebf0cdd9c"
	_, err := Post(url, params, map[string]string{"Content-Type": "application/json"}, nil, nil)
	if nil != err {
		return err
	}
	return nil
}
