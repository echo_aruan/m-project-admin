package redis

import (
	"testing"
)

func TestGetRedisClient(t *testing.T) {
	c, _ := NewClient("127.0.0.1:6379", "")
	t.Log(c)
}

func TestRedisClient_LPush(t *testing.T) {
	c, _ := NewClient("127.0.0.1:6379", "")

	err := c.LPush("test", "hello!")

	if nil != err {
		t.Fatal(err)
	}
}

func TestRedisClient_LPop(t *testing.T) {
	c, _ := NewClient("127.0.0.1:6379", "")

	data, err := c.LPop("test")

	if nil != err {
		t.Fatal(err)
	}

	t.Log(data)
}

func TestRedisClient_RPush(t *testing.T) {
	c, _ := NewClient("127.0.0.1:6379", "")

	err := c.RPush("test", "hello!")

	if nil != err {
		t.Fatal(err)
	}
}

func TestRedisClient_RPop(t *testing.T) {
	c, _ := NewClient("127.0.0.1:6379", "")

	data, err := c.RPop("test")

	if nil != err {
		t.Fatal(err)
	}

	t.Log(data)
}
