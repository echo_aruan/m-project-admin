package manager

import (
	"gopkg.in/mgo.v2/bson"

	"m-project-admin/app/driver"
	"m-project-admin/app/model"
)

const AppsTable = "apps"

func SaveApp(template model.App) (bool, error) {
	session, db, err := driver.GetMongoDB()
	if err != nil {
		return false, err
	}
	defer session.Close()
	c := session.DB(db).C(AppsTable)
	err = c.Insert(&template)
	if err != nil {
		return false, err
	}
	return true, nil
}

func GetAppCount(appId string, appName string) (int, error) {
	session, db, err := driver.GetMongoDB()
	if err != nil {
		return 0, err
	}
	defer session.Close()
	c := session.DB(db).C(AppsTable)

	query := bson.M{}
	if appId != "" {
		query["_id"] = bson.ObjectIdHex(appId)
	}
	if appName != "" {
		query["app_name"] = bson.M{"$regex": bson.RegEx{Pattern: appName, Options: "i"}}
	}

	count, err := c.Find(query).Count()
	return count, nil
}

func GetAppList(appId string, appName string, skip int, pageCount int) ([]model.App, error) {
	session, db, err := driver.GetMongoDB()
	if err != nil {
		return nil, err
	}
	defer session.Close()
	c := session.DB(db).C(AppsTable)

	var rows []model.App
	query := bson.M{}
	if appId != "" {
		query["_id"] = bson.ObjectIdHex(appId)
	}
	if appName != "" {
		query["app_name"] = bson.M{"$regex": bson.RegEx{Pattern: appName, Options: "i"}}
	}
	err = c.Find(query).Skip(skip).Limit(pageCount).Sort("-create_time").All(&rows)
	if err != nil {
		return nil, err
	}

	return rows, nil
}

func GetAllAppList() ([]model.App, error) {
	session, db, err := driver.GetMongoDB()
	if err != nil {
		return nil, err
	}
	defer session.Close()
	c := session.DB(db).C(AppsTable)

	var rows []model.App
	query := bson.M{}
	err = c.Find(query).All(&rows)
	if err != nil {
		return nil, err
	}

	return rows, nil
}

func GetAppByKey(key string) (model.App, bool, error) {
	session, db, err := driver.GetMongoDB()
	if err != nil {
		return model.App{}, false, err
	}
	defer session.Close()
	c := session.DB(db).C(AppsTable)

	var app model.App
	query := bson.M{"app_key": key}
	err = c.Find(query).One(&app)
	if err != nil {
		return app, false, err
	}

	return app, true, nil
}

func GetAppById(dataid string) (model.App, bool, error) {
	session, db, err := driver.GetMongoDB()
	if err != nil {
		return model.App{}, false, err
	}
	defer session.Close()
	c := session.DB(db).C(AppsTable)

	var app model.App
	query := bson.M{"_id": bson.ObjectIdHex(dataid)}
	err = c.Find(query).One(&app)
	if err != nil {
		return app, false, err
	}

	return app, true, nil
}

func GetAppByName(title string) (model.App, bool, error) {
	session, db, err := driver.GetMongoDB()
	if err != nil {
		return model.App{}, false, err
	}
	defer session.Close()
	c := session.DB(db).C(AppsTable)

	var app model.App
	query := bson.M{"app_name": title}
	err = c.Find(query).One(&app)
	if err != nil {
		return app, false, err
	}

	return app, true, nil
}

func UpdateApp(dataid string, app map[string]interface{}) (bool, error) {
	session, db, err := driver.GetMongoDB()
	if err != nil {
		return false, err
	}
	defer session.Close()
	c := session.DB(db).C(AppsTable)

	query := bson.M{"_id": bson.ObjectIdHex(dataid)}
	data := bson.M{"$set": app}
	err = c.Update(query, data)
	if err != nil {
		return false, err
	}

	return true, nil
}

func DeleteApp(dataid string) (bool, error) {
	session, db, err := driver.GetMongoDB()
	if err != nil {
		return false, err
	}
	defer session.Close()
	c := session.DB(db).C(AppsTable)

	id := bson.ObjectIdHex(dataid)
	_, err = c.RemoveAll(bson.M{"_id": id})
	if err != nil {
		return false, err
	}

	return true, nil
}
