package manager

import (
	"gopkg.in/mgo.v2/bson"

	"errors"
	"m-project-admin/app/driver"
	"m-project-admin/app/model"
)

const RoleTable = "roles"

func SaveRole(task model.Role) (bool, error) {
	session, db, err := driver.GetMongoDB()
	if err != nil {
		return false, err
	}
	defer session.Close()
	c := session.DB(db).C(RoleTable)

	err = c.Insert(&task)
	if err != nil {
		return false, err
	}
	return true, nil
}

func GetRoleCount(dataid string, name string) (int, error) {
	session, db, err := driver.GetMongoDB()
	if err != nil {
		return 0, err
	}
	defer session.Close()
	c := session.DB(db).C(RoleTable)

	query := bson.M{}
	if dataid != "" {
		query["_id"] = bson.ObjectIdHex(dataid)
	}
	if name != "" {
		query["name"] = bson.M{"$regex": bson.RegEx{Pattern: name, Options: "i"}}
	}

	count, err := c.Find(query).Count()
	return count, nil
}

func GetRoleList(dataid string, name string, skip int, pageCount int) ([]model.Role, error) {
	session, db, err := driver.GetMongoDB()
	if err != nil {
		return nil, err
	}
	defer session.Close()
	c := session.DB(db).C(RoleTable)

	var rows []model.Role
	query := bson.M{}
	if dataid != "" {
		query["_id"] = bson.ObjectIdHex(dataid)
	}
	if name != "" {
		query["name"] = bson.M{"$regex": bson.RegEx{Pattern: name, Options: "i"}}
	}
	err = c.Find(query).Skip(skip).Limit(pageCount).Sort("-create_time").All(&rows)
	if err != nil {
		return nil, err
	}

	return rows, nil
}

func GetAllRoles() ([]model.Role, error) {
	session, db, err := driver.GetMongoDB()
	if err != nil {
		return nil, err
	}
	defer session.Close()
	c := session.DB(db).C(RoleTable)

	var rows []model.Role
	query := bson.M{}
	err = c.Find(query).All(&rows)
	if err != nil {
		return nil, err
	}

	return rows, nil
}

func GetAllRolesByIds(dataids []string) ([]model.Role, error) {
	if len(dataids) == 0 {
		return nil, errors.New("dataids is empty")
	}
	session, db, err := driver.GetMongoDB()
	if err != nil {
		return nil, err
	}
	defer session.Close()
	c := session.DB(db).C(RoleTable)

	var rows []model.Role
	ids := make([]bson.ObjectId, 0)
	for _, dataid := range dataids {
		ids = append(ids, bson.ObjectIdHex(dataid))
	}
	queryIds := bson.M{"$in": ids}
	query := bson.M{"_id": queryIds}
	err = c.Find(query).All(&rows)
	if err != nil {
		return nil, err
	}

	return rows, nil
}

func GetRoleById(dataid string) (model.Role, error) {
	session, db, err := driver.GetMongoDB()
	if err != nil {
		return model.Role{}, err
	}
	defer session.Close()
	c := session.DB(db).C(RoleTable)

	var role model.Role
	query := bson.M{"_id": bson.ObjectIdHex(dataid)}
	err = c.Find(query).One(&role)
	return role, nil
}

func GetRoleByAppIdName(appId string, name string) (model.Role, error) {
	session, db, err := driver.GetMongoDB()
	if err != nil {
		return model.Role{}, err
	}
	defer session.Close()
	c := session.DB(db).C(RoleTable)

	var role model.Role
	query := bson.M{"app_id": appId, "name": name}
	err = c.Find(query).One(&role)
	return role, nil
}

func UpdateRole(dataid string, role bson.M) (bool, error) {
	session, db, err := driver.GetMongoDB()
	if err != nil {
		return false, err
	}
	defer session.Close()
	c := session.DB(db).C(RoleTable)

	query := bson.M{"_id": bson.ObjectIdHex(dataid)}
	data := bson.M{"$set": role}
	err = c.Update(query, data)
	if err != nil {
		return false, err
	}

	return true, nil
}

func DeleteRole(dataid string) (bool, error) {
	session, db, err := driver.GetMongoDB()
	if err != nil {
		return false, err
	}
	defer session.Close()
	c := session.DB(db).C(RoleTable)

	id := bson.ObjectIdHex(dataid)
	_, err = c.RemoveAll(bson.M{"_id": id})
	if err != nil {
		return false, err
	}

	return true, nil
}
