package manager

import (
	"gopkg.in/mgo.v2/bson"

	"errors"
	"m-project-admin/app/driver"
	"m-project-admin/app/model"
)

const PremissionTable = "premissions"

func SavePremission(premission model.Premission) (bool, error) {
	session, db, err := driver.GetMongoDB()
	if err != nil {
		return false, err
	}
	defer session.Close()
	c := session.DB(db).C(PremissionTable)

	err = c.Insert(&premission)
	if err != nil {
		return false, err
	}
	return true, nil
}

func GetPremissionCount(dataid string, key string, name string) (int, error) {
	session, db, err := driver.GetMongoDB()
	if err != nil {
		return 0, err
	}
	defer session.Close()
	c := session.DB(db).C(PremissionTable)

	query := bson.M{}
	if dataid != "" {
		query["_id"] = bson.ObjectIdHex(dataid)
	}
	if key != "" {
		query["key"] = bson.M{"$regex": bson.RegEx{Pattern: key, Options: "i"}}
	}
	if name != "" {
		query["name"] = bson.M{"$regex": bson.RegEx{Pattern: name, Options: "i"}}
	}

	count, err := c.Find(query).Count()
	return count, nil
}

func GetPremissionList(dataid string, key string, name string, skip int, pageCount int) ([]model.Premission, error) {
	session, db, err := driver.GetMongoDB()
	if err != nil {
		return nil, err
	}
	defer session.Close()
	c := session.DB(db).C(PremissionTable)

	var rows []model.Premission
	query := bson.M{}
	if dataid != "" {
		query["_id"] = bson.ObjectIdHex(dataid)
	}
	if key != "" {
		query["key"] = bson.M{"$regex": bson.RegEx{Pattern: key, Options: "i"}}
	}
	if name != "" {
		query["name"] = bson.M{"$regex": bson.RegEx{Pattern: name, Options: "i"}}
	}
	err = c.Find(query).Skip(skip).Limit(pageCount).Sort("-create_time").All(&rows)
	if err != nil {
		return nil, err
	}

	return rows, nil
}

func GetAllPremissionsByIds(dataids []string) ([]model.Premission, error) {
	if len(dataids) == 0 {
		return nil, errors.New("dataids is empty")
	}
	session, db, err := driver.GetMongoDB()
	if err != nil {
		return nil, err
	}
	defer session.Close()
	c := session.DB(db).C(PremissionTable)

	var rows []model.Premission
	ids := make([]bson.ObjectId, 0)
	for _, dataid := range dataids {
		ids = append(ids, bson.ObjectIdHex(dataid))
	}
	queryIds := bson.M{"$in": ids}
	query := bson.M{"_id": queryIds}
	err = c.Find(query).All(&rows)
	if err != nil {
		return nil, err
	}

	return rows, nil
}

func GetAllPremissionByAppId(appId string) ([]model.Premission, error) {
	session, db, err := driver.GetMongoDB()
	if err != nil {
		return nil, err
	}
	defer session.Close()
	c := session.DB(db).C(PremissionTable)

	var rows []model.Premission
	query := bson.M{"app_id": appId}
	err = c.Find(query).All(&rows)
	if err != nil {
		return nil, err
	}

	return rows, nil
}

func GetPremissionByAppIdKey(appId string, key string) (model.Premission, error) {
	session, db, err := driver.GetMongoDB()
	if err != nil {
		return model.Premission{}, err
	}
	defer session.Close()
	c := session.DB(db).C(PremissionTable)

	var premission model.Premission
	query := bson.M{"app_id": appId, "key": key}
	err = c.Find(query).One(&premission)
	return premission, nil
}

func GetPremissionById(dataid string) (model.Premission, error) {
	session, db, err := driver.GetMongoDB()
	if err != nil {
		return model.Premission{}, err
	}
	defer session.Close()
	c := session.DB(db).C(PremissionTable)

	var task model.Premission
	query := bson.M{"_id": bson.ObjectIdHex(dataid)}
	err = c.Find(query).One(&task)
	return task, nil
}

func GetPremissionByKey(key string) (model.Premission, error) {
	session, db, err := driver.GetMongoDB()
	if err != nil {
		return model.Premission{}, err
	}
	defer session.Close()
	c := session.DB(db).C(PremissionTable)

	var task model.Premission
	query := bson.M{"key": key}
	err = c.Find(query).One(&task)
	return task, nil
}

func UpdatePremission(dataid string, task bson.M) (bool, error) {
	session, db, err := driver.GetMongoDB()
	if err != nil {
		return false, err
	}
	defer session.Close()
	c := session.DB(db).C(PremissionTable)

	query := bson.M{"_id": bson.ObjectIdHex(dataid)}
	data := bson.M{"$set": task}
	err = c.Update(query, data)
	if err != nil {
		return false, err
	}

	return true, nil
}

func DeletePremission(dataid string) (bool, error) {
	session, db, err := driver.GetMongoDB()
	if err != nil {
		return false, err
	}
	defer session.Close()
	c := session.DB(db).C(PremissionTable)

	id := bson.ObjectIdHex(dataid)
	_, err = c.RemoveAll(bson.M{"_id": id})
	if err != nil {
		return false, err
	}

	return true, nil
}
