package manager

import (
	"gopkg.in/mgo.v2/bson"

	"m-project-admin/app/driver"
	"m-project-admin/app/model"
)

const AccountTable = "accounts"

func SaveAccount(task model.Account) (bool, error) {
	session, db, err := driver.GetMongoDB()
	if err != nil {
		return false, err
	}
	defer session.Close()
	c := session.DB(db).C(AccountTable)

	err = c.Insert(&task)
	if err != nil {
		return false, err
	}
	return true, nil
}

func GetAccountCount(dataid string, name string, account string) (int, error) {
	session, db, err := driver.GetMongoDB()
	if err != nil {
		return 0, err
	}
	defer session.Close()
	c := session.DB(db).C(AccountTable)

	query := bson.M{}
	if dataid != "" {
		query["_id"] = bson.ObjectIdHex(dataid)
	}
	if name != "" {
		query["name"] = bson.M{"$regex": bson.RegEx{Pattern: name, Options: "i"}}
	}
	if name != "" {
		query["account"] = bson.M{"$regex": bson.RegEx{Pattern: account, Options: "i"}}
	}

	count, err := c.Find(query).Count()
	return count, nil
}

func GetAccountList(dataid string, name string, account string, skip int, pageCount int) ([]model.Account, error) {
	session, db, err := driver.GetMongoDB()
	if err != nil {
		return nil, err
	}
	defer session.Close()
	c := session.DB(db).C(AccountTable)

	var rows []model.Account
	query := bson.M{}
	if dataid != "" {
		query["_id"] = bson.ObjectIdHex(dataid)
	}
	if name != "" {
		query["name"] = bson.M{"$regex": bson.RegEx{Pattern: name, Options: "i"}}
	}
	if name != "" {
		query["account"] = bson.M{"$regex": bson.RegEx{Pattern: account, Options: "i"}}
	}
	err = c.Find(query).Skip(skip).Limit(pageCount).Sort("-create_time").All(&rows)
	if err != nil {
		return nil, err
	}

	return rows, nil
}

func GetAllAccounts() ([]model.Account, error) {
	session, db, err := driver.GetMongoDB()
	if err != nil {
		return nil, err
	}
	defer session.Close()
	c := session.DB(db).C(AccountTable)

	var rows []model.Account
	query := bson.M{}
	err = c.Find(query).All(&rows)
	if err != nil {
		return nil, err
	}

	return rows, nil
}

func GetAccountById(dataid string) (model.Account, error) {
	session, db, err := driver.GetMongoDB()
	if err != nil {
		return model.Account{}, err
	}
	defer session.Close()
	c := session.DB(db).C(AccountTable)

	var role model.Account
	query := bson.M{"_id": bson.ObjectIdHex(dataid)}
	err = c.Find(query).One(&role)
	return role, nil
}

func GetAccountByAccount(account string) (model.Account, error) {
	session, db, err := driver.GetMongoDB()
	if err != nil {
		return model.Account{}, err
	}
	defer session.Close()
	c := session.DB(db).C(AccountTable)

	var role model.Account
	query := bson.M{"account": account}
	err = c.Find(query).One(&role)
	return role, nil
}

func GetAccountByName(name string) (model.Account, error) {
	session, db, err := driver.GetMongoDB()
	if err != nil {
		return model.Account{}, err
	}
	defer session.Close()
	c := session.DB(db).C(AccountTable)

	var role model.Account
	query := bson.M{"name": name}
	err = c.Find(query).One(&role)
	return role, nil
}

func UpdateAccount(dataid string, role bson.M) (bool, error) {
	session, db, err := driver.GetMongoDB()
	if err != nil {
		return false, err
	}
	defer session.Close()
	c := session.DB(db).C(AccountTable)

	query := bson.M{"_id": bson.ObjectIdHex(dataid)}
	data := bson.M{"$set": role}
	err = c.Update(query, data)
	if err != nil {
		return false, err
	}

	return true, nil
}

func DeleteAccount(dataid string) (bool, error) {
	session, db, err := driver.GetMongoDB()
	if err != nil {
		return false, err
	}
	defer session.Close()
	c := session.DB(db).C(AccountTable)

	id := bson.ObjectIdHex(dataid)
	_, err = c.RemoveAll(bson.M{"_id": id})
	if err != nil {
		return false, err
	}

	return true, nil
}
