package manager

// 部门枚举
const (
	DepartmentDbj  = 1
	DepartmentDjk  = 2
	DepartmentWa   = 3
	DepartmentJszx = 4
)

var DepartmentDesc = map[int]string{
	DepartmentDbj:  "大保健",
	DepartmentDjk:  "大健康",
	DepartmentWa:   "微爱",
	DepartmentJszx: "技术中心",
}
