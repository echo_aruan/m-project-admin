package manager

import (
	"errors"
	"fmt"
	"gopkg.in/mgo.v2/bson"
	"m-project-admin/app/driver"
	"m-project-admin/app/lib/redis"
	"m-project-admin/app/model"
	"m-project-admin/app/utils"
	"time"
)

// Sso登录检查逻辑
func SsoCheckLogin(appId string, account string, password string) (model.Account, bool, string) {
	session, db, err := driver.GetMongoDB()
	if err != nil {
		return model.Account{}, false, err.Error()
	}
	defer session.Close()

	c := session.DB(db).C(AccountTable)

	var accountObj model.Account
	query := bson.M{"account": account}
	err = c.Find(query).One(&accountObj)
	if err != nil {
		return accountObj, false, "账号或密码错误"
	}

	// 判断是否有权限登录当前APP
	if len(accountObj.Apps) == 0 {
		return accountObj, false, "Sorry 😞 您没权限登录当前系统"
	}
	isAccessLogin := false
	for _, id := range accountObj.Apps {
		if id == appId {
			isAccessLogin = true
		}
	}
	if !isAccessLogin {
		return accountObj, false, "Sorry 😞 您没权限登录当前系统"
	}

	tmpPwd := utils.Sha1s([]byte(password))
	pwd := accountObj.Password
	if tmpPwd != pwd {
		return accountObj, false, "账号或密码错误"
	}

	// 判断是否被禁止登录
	if accountObj.Status == 1 {
		return accountObj, false, "您已被禁止登录"
	}

	return accountObj, true, "成功"
}

func SetTokenAccount(token string, accountId string, ttl time.Duration) error {
	uri := utils.GetSectionValue("ucenter-data-redis", "uri", "")
	if uri == "" {
		return errors.New("redis地址解析错误")
	}

	redisClient, err := redis.NewClient(uri, "")
	if err != nil {
		return err
	}
	key1 := fmt.Sprintf("sso_%s_token", accountId)
	key2 := fmt.Sprintf("sso_%s_account", token)
	redisClient.Client.Set(key1, token, ttl).Err()
	err = redisClient.Client.Set(key2, accountId, ttl).Err()
	return err
}

func DelTokenAccountByToken(token string) error {
	uri := utils.GetSectionValue("ucenter-data-redis", "uri", "")
	if uri == "" {
		return errors.New("redis地址解析错误")
	}
	redisClient, err := redis.NewClient(uri, "")
	if err != nil {
		return err
	}

	key1 := fmt.Sprintf("sso_%s_account", token)
	accountId, err := redisClient.Client.Get(key1).Result()

	// 删除accountId
	err = redisClient.Client.Del(key1).Err()

	// 删除token
	key2 := fmt.Sprintf("sso_%s_token", accountId)
	redisClient.Client.Del(key2).Err()

	return err
}

func DelTokenAccountByAccountId(accountId string) error {
	uri := utils.GetSectionValue("ucenter-data-redis", "uri", "")
	if uri == "" {
		return errors.New("redis地址解析错误")
	}
	redisClient, err := redis.NewClient(uri, "")
	if err != nil {
		return err
	}

	key1 := fmt.Sprintf("sso_%s_token", accountId)
	token, err := redisClient.Client.Get(key1).Result()

	// 删除token
	err = redisClient.Client.Del(key1).Err()

	// 删除accountId
	if err != nil {
		key2 := fmt.Sprintf("sso_%s_account", token)
		redisClient.Client.Del(key2).Err()
	}

	return err
}

func GetAccountIdByToken(token string) (string, error) {
	uri := utils.GetSectionValue("ucenter-data-redis", "uri", "")
	if uri == "" {
		return "", errors.New("redis地址解析错误")
	}
	redisClient, err := redis.NewClient(uri, "")
	if err != nil {
		return "", err
	}
	key := fmt.Sprintf("sso_%s_account", token)
	return redisClient.Client.Get(key).Result()
}

func GetTokenByAccountId(accountId string) (string, error) {
	uri := utils.GetSectionValue("ucenter-data-redis", "uri", "")
	if uri == "" {
		return "", errors.New("redis地址解析错误")
	}
	redisClient, err := redis.NewClient(uri, "")
	if err != nil {
		return "", err
	}

	key := fmt.Sprintf("sso_%s_token", accountId)
	return redisClient.Client.Get(key).Result()
}

func FormatAccountPhoto(photo string) string {
	if photo != "" {
		return photo
	}

	defaultPhoto := utils.GetSectionValue("server", "default_photo", "")
	if defaultPhoto == "" {
		return ""
	}
	return defaultPhoto
}
