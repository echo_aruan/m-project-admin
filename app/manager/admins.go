package manager

import (
	"gopkg.in/mgo.v2/bson"

	"m-project-admin/app/driver"
	"m-project-admin/app/model"
	"m-project-admin/app/utils"
)

const AdminTable = "admins"

// 登录检查逻辑
func CheckLogin(account string, password string) (model.Admin, bool, string) {
	session, db, err := driver.GetMongoDB()
	if err != nil {
		return model.Admin{}, false, err.Error()
	}
	defer session.Close()

	c := session.DB(db).C(AdminTable)

	var admin model.Admin
	query := bson.M{"account": account}
	err = c.Find(query).One(&admin)
	if err != nil {
		return admin, false, "账号或密码错误"
	}

	tmpPwd := utils.Sha1s([]byte(password))
	pwd := admin.Password
	if tmpPwd != pwd {
		return admin, false, "账号或密码错误"
	}

	// 判断是否被禁止登录
	if admin.Status == 1 {
		return admin, false, "您已被禁止登录"
	}

	return admin, true, "成功"
}

func SaveAdmin(admin model.Admin) (bool, error) {
	session, db, err := driver.GetMongoDB()
	if err != nil {
		return false, err
	}
	defer session.Close()
	c := session.DB(db).C(AdminTable)
	err = c.Insert(&admin)
	if err != nil {
		return false, err
	}
	return true, nil
}

func UpdateAdmin(dataid string, admin bson.M) (bool, error) {
	session, db, err := driver.GetMongoDB()
	if err != nil {
		return false, err
	}
	defer session.Close()
	c := session.DB(db).C(AdminTable)

	query := bson.M{"_id": bson.ObjectIdHex(dataid)}

	data := bson.M{"$set": admin}
	err = c.Update(query, data)
	if err != nil {
		return false, err
	}

	return true, nil
}

func DeleteAdmin(dataid string) (bool, error) {
	session, db, err := driver.GetMongoDB()
	if err != nil {
		return false, err
	}
	defer session.Close()
	c := session.DB(db).C(AdminTable)

	id := bson.ObjectIdHex(dataid)
	_, err = c.RemoveAll(bson.M{"_id": id})
	if err != nil {
		return false, err
	}

	return true, nil
}

func GetAdminByAccount(account string) (model.Admin, bool, error) {
	session, db, err := driver.GetMongoDB()
	if err != nil {
		return model.Admin{}, false, err
	}
	defer session.Close()
	c := session.DB(db).C(AdminTable)

	var admin model.Admin
	query := bson.M{"account": account}
	c.Find(query).One(&admin)
	return admin, true, nil
}

func GetAdminById(dataid string) (model.Admin, bool, error) {
	session, db, err := driver.GetMongoDB()
	if err != nil {
		return model.Admin{}, false, err
	}
	defer session.Close()
	c := session.DB(db).C(AdminTable)

	var admin model.Admin
	query := bson.M{"_id": bson.ObjectIdHex(dataid)}
	err = c.Find(query).One(&admin)
	if err != nil {
		return admin, false, err
	}

	return admin, true, nil
}

func GetAdminCount() (int, error) {
	session, db, err := driver.GetMongoDB()
	if err != nil {
		return 0, err
	}
	defer session.Close()
	c := session.DB(db).C(AdminTable)

	count, err := c.Count()
	return count, nil
}

func GetAdminList(skip int, pageCount int) ([]model.Admin, error) {
	session, db, err := driver.GetMongoDB()
	if err != nil {
		return nil, err
	}
	defer session.Close()
	c := session.DB(db).C(AdminTable)

	var rows []model.Admin
	query := bson.M{}
	err = c.Find(query).Skip(skip).Limit(pageCount).All(&rows)
	if err != nil {
		return nil, err
	}

	return rows, nil
}
