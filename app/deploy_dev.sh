#!/bin/bash

## 编译
echo "开始编译 ..."
rm -rf main
go build main.go
echo "编译完成 ..."

## 创建临时文件夹
echo "pack deploy files ..."
if [ ! -d "./tmp" ]; then
  mkdir ./tmp
fi

## 开始部署
echo "开始部署 ..."
mv main ./tmp
cp -R 404.html ./tmp
cp -R config ./tmp
cp -R views ./tmp
cp -R static ./tmp
rm ./tmp/config/conf.ini
rm ./tmp/config/conf_pro.ini
mv ./tmp/config/conf_dev.ini ./tmp/config/conf.ini
cd ./tmp
tar zcvf ageis-center.tar.gz ./*
cp ageis-center.tar.gz /root/deploy
cd ..
rm -rf ./tmp
rm -rf ageis-center.tar.gz
echo "部署完成 ..."