package controller

import (
	"net/http"

	"github.com/labstack/echo"

	"m-project-admin/app/manager"
	"m-project-admin/app/utils"
	"time"
)

// 首页
func Index(c echo.Context) error {
	loginAdminInfo := manager.GetLoginAdminInfo(&c)

	data := make(map[string]interface{})
	data["loginAdminInfo"] = loginAdminInfo
	data["env_desc"] = utils.GetEnvDesc()
	return c.Render(http.StatusOK, "index.html", data)
}

func Welcome(c echo.Context) error {
	loginAdminInfo := manager.GetLoginAdminInfo(&c)

	// app数
	appCount, _ := manager.GetAppCount("", "")

	// 权限数
	premissionCount, _ := manager.GetPremissionCount("", "", "")

	// 角色数
	roleCount, _ := manager.GetRoleCount("", "")

	// 账号数
	accountCount, _ := manager.GetAccountCount("", "", "")

	data := make(map[string]interface{})
	data["loginAdminInfo"] = loginAdminInfo
	data["appCount"] = appCount
	data["premissionCount"] = premissionCount
	data["roleCount"] = roleCount
	data["accountCount"] = accountCount
	data["now_date"] = time.Unix(time.Now().Unix(), 0).Format("2006-01-02 15:04:05")
	data["env_desc"] = utils.GetEnvDesc()
	return c.Render(http.StatusOK, "welcome.html", data)
}
