#!/bin/bash

git pull origin master

echo "start build ..."
rm -rf main
go build main.go
echo "build finish ..."

echo "pack deploy files ..."
if [ ! -d "./tmp" ]; then
  mkdir ./tmp
fi
cp -R main ./tmp
cp -R 404.html ./tmp
cp -R config ./tmp
cp -R views ./tmp
cp -R static ./tmp
rm ./tmp/config/conf.ini
rm ./tmp/config/conf_dev.ini
mv ./tmp/config/conf_pro.ini ./tmp/config/conf.ini
echo "pack is done ..."

echo "pack upload go3 is doing ..."
cd ./tmp
rm -rf ageis-center.tar.gz
tar zcvf ageis-center.tar.gz ./*
scp ageis-center.tar.gz 10.111.11.255:/root/ageis-center
echo "pack upload go3 is done ..."