package router

import (
	"github.com/labstack/echo"

	"m-project-admin/app/controller"
)

func InitRouter(e *echo.Echo) {

	e.GET("/", controller.Index)
	e.GET("/welcome", controller.Welcome)

	e.GET("/login", controller.Login)
	e.POST("/doLogin", controller.DoLogin)
	e.GET("/logout", controller.Logout)

	e.GET("/homeList", controller.HomeList)
	e.GET("/addApp", controller.AddApp)
	e.GET("/updateApp", controller.UpdateApp)
	e.POST("/doAddApp", controller.DoAddApp)
	e.POST("/doUpdateApp", controller.DoUpdateApp)
	e.POST("/doDeleteApp", controller.DoDeleteApp)
	e.POST("/doDeleteAppBatch", controller.DoDeleteAppBatch)

	e.GET("/addPremission", controller.AddPremission)
	e.GET("/updatePremission", controller.UpdatePremission)
	e.POST("/doAddPremission", controller.DoAddPremission)
	e.POST("/doUpdatePremission", controller.DoUpdatePremission)
	e.POST("/doDeletePremission", controller.DoDeletePremission)
	e.POST("/doDeletePremissionBatch", controller.DoDeletePremissionBatch)
	e.GET("/premissionList", controller.PremissionList)

	e.GET("/addRole", controller.AddRole)
	e.GET("/updateRole", controller.UpdateRole)
	e.GET("/updateRolePremission", controller.UpdateRolePremission)
	e.POST("/doAddRole", controller.DoAddRole)
	e.POST("/doUpdateRole", controller.DoUpdateRole)
	e.POST("/doUpdateRolePremission", controller.DoUpdateRolePremission)
	e.POST("/doDeleteRole", controller.DoDeleteRole)
	e.POST("/doDeleteRoleBatch", controller.DoDeleteRoleBatch)
	e.GET("/roleList", controller.RoleList)

	e.GET("/addAccount", controller.AddAccount)
	e.GET("/updateAccount", controller.UpdateAccount)
	e.POST("/doAddAccount", controller.DoAddAccount)
	e.POST("/doUpdateAccount", controller.DoUpdateAccount)
	e.GET("/updateAccountApp", controller.UpdateAccountApp)
	e.POST("/doUpdateAccountApp", controller.DoUpdateAccountApp)
	e.GET("/updateAccountRole", controller.UpdateAccountRole)
	e.POST("/doUpdateAccountRole", controller.DoUpdateAccountRole)
	e.POST("/doDeleteAccount", controller.DoDeleteAccount)
	e.POST("/doDeleteAccountBatch", controller.DoDeleteAccountBatch)
	e.POST("/doSearchToken", controller.DoSearchToken)
	e.GET("/accountList", controller.AccountList)

	e.GET("/addAdmin", controller.AddAdmin)
	e.GET("/updateAdmin", controller.UpdateAdmin)
	e.GET("/adminList", controller.AdminList)
	e.POST("/doAddAdmin", controller.DoAddAdmin)
	e.POST("/doUpdateAdmin", controller.DoUpdateAdmin)
	e.POST("/doDeleteAdmin", controller.DoDeleteAdmin)
	e.POST("/doDeleteAdminBatch", controller.DoDeleteAdminBatch)

	e.GET("/ssoLogin", controller.SsoLogin)
	e.POST("/ssoDoLogin", controller.SsoDoLogin)
	e.GET("/ssoLogout", controller.SsoLogout)
	e.POST("/ssoCheck", controller.SsoCheck)
	e.POST("/ssoAccountProfile", controller.SsoAccountProfile)
	e.POST("/ssoAccountProfiles", controller.SsoAccountProfiles)
	e.POST("/ssoSearchAdmin", controller.SsoSearchAdmin)
	e.POST("/ssoAccountList", controller.SsoAccountList)
	e.POST("/ssoSearchAccountPremissions", controller.SsoSearchAccountPremissions)
}
