package model

import (
	"gopkg.in/mgo.v2/bson"
)

// 业务线
const (
	HuZhu = "互助"
	EBao  = "e保"
	Bp    = "平台"
)

var BusinessList = []map[string]string{
	{
		"key":  "huzhu",
		"name": HuZhu,
	},
	{
		"key":  "ebao",
		"name": EBao,
	},
	{
		"key":  "bp",
		"name": Bp,
	},
}

type Account struct {
	Id            bson.ObjectId `bson:"_id"`
	Name          string        `json:"name"`
	Account       string        `json:"account"`
	Password      string        `json:"password"`
	Photo         string        `json:"photo"`
	Is_Super      int           `json:"is_super"`
	Department    int           `json:"department"`
	BusinessLines []string      `bson:"business_lines"`
	Status        int           `json:"status"`
	Apps          []string      `json:"apps"`
	Roles         []string      `json:"roles"`
	Adminid       string        `json:"adminid"`
	Create_time   int64         `json:"create_time"`
	Update_time   int64         `json:"update_time"`
}
