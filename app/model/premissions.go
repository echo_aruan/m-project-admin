package model

import (
	"gopkg.in/mgo.v2/bson"
)

type Premission struct {
	Id          bson.ObjectId `bson:"_id"`
	Key         string        `json:"key"`
	Name        string        `json:"name"`
	App_id      string        `json:"app_id"`
	Adminid     string        `json:"adminid"`
	Create_time int64         `json:"create_time"`
	Update_time int64         `json:"update_time"`
}
