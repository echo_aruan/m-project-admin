package model

import (
	"gopkg.in/mgo.v2/bson"
)

type Admin struct {
	Id          bson.ObjectId `bson:"_id"`
	Account     string        `json:"account"`
	Phone       string        `json:"phone"`
	Name        string        `json:"name"`
	Password    string        `json:"password"`
	Status      int           `json:"status"`   // 是否禁用:0不是 1是
	Is_super    int           `json:"is_super"` // 是否是超级管理员:0不是 1是
	Create_time int64         `json:"create_time"`
	Update_time int64         `json:"update_time"`
}
