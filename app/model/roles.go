package model

import (
	"gopkg.in/mgo.v2/bson"
)

type Role struct {
	Id          bson.ObjectId `bson:"_id"`
	Name        string        `json:"name"`
	Desc        string        `json:"desc"`
	App_id      string        `json:"app_id"`
	Premissions []string      `json:"premissions"`
	Adminid     string        `json:"adminid"`
	Create_time int64         `json:"create_time"`
	Update_time int64         `json:"update_time"`
}
