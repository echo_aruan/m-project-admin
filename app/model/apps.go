package model

import (
	"gopkg.in/mgo.v2/bson"
)

type App struct {
	Id           bson.ObjectId `bson:"_id"`
	App_name     string        `json:"app_name"`
	Callback_url string        `json:"callback_url"`
	Adminid      string        `json:"adminid"`
	App_key      string        `json:"app_key"`
	App_secret   string        `json:"app_secret"`
	Status       int           `json:"status"` // 0 正常  1禁用
	Create_time  int64         `json:"create_time"`
	Update_time  int64         `json:"update_time"`
}
