package entity

type Response struct {
	En   int         `json:"en"`
	Em   string      `json:"em"`
	Data interface{} `json:"data"`
}
