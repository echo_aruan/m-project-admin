package driver

import (
	"errors"

	"gopkg.in/mgo.v2"

	"m-project-admin/app/utils"
)

// GetMongoDB
func GetMongoDB() (*mgo.Session, string, error) {
	uri := utils.GetSectionValue("mongo", "uri", "")
	if uri == "" {
		return nil, "", errors.New("mongo地址解析错误")
	}
	db := utils.GetSectionValue("mongo", "db", "")
	if db == "" {
		return nil, "", errors.New("mongo db解析错误")
	}
	session, err := mgo.Dial(uri)
	session.SetMode(mgo.Monotonic, true)
	return session, db, err
}
